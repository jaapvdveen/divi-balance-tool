﻿const api = "https://api.diviscan.io/address/";
const diviscan = "https://diviscan.io/address/";
var addresses = localStorage.getItem('addresses') ? JSON.parse(localStorage.getItem('addresses')) :[];
var total_sum = 0;

const form = document.getElementById('address-form');
const addresslist = document.getElementById('addresses');
const btn_clear = document.getElementById('btn_clear');
const input = document.getElementById('newaddr');
const total_balance = document.getElementById('total_balance');

const addToList = (addr, newAddr) => {

	$.get(api+addr,function(data){
		// console.log('Balance: '+data.balance_info.balance);
		if(data.balance_info.balance){
			var balance = data.balance_info.balance / 100000000;

			var li = document.createElement('li');

			li.id = addr;

			var link = document.createElement('a');
			link.setAttribute('href', diviscan+addr);
			link.setAttribute('target', '_blank');
			link.setAttribute('title', 'Click to open details in new window');
			link.textContent = addr.substr(0,4)+'***'+addr.slice(-4)+' ';

			var icon = document.createElement('i');
			icon.className = "fas fa-external-link-alt";

			link.appendChild(icon);

			li.appendChild(link);

			var span = document.createElement('span');
			span.className = "balance right";
			span.textContent = balance.toLocaleString('en');
			//console.log(balance.toLocaleString('en'));

			li.appendChild(span);

			addresslist.appendChild(li);
			if(newAddr === true){
				// console.log('Permission to add as new to addresses');
				addresses.push(addr);
				localStorage.setItem('addresses', JSON.stringify(addresses));
			}

			total_sum += balance;
			total_balance.textContent = total_sum.toLocaleString('en');

		}else{
			alert('Sorry, that is not a valid address.');
		}
	},"json");
}

$.ajaxSetup({ async: false });
addresses.forEach(addr =>{
	addToList(addr);
});
$.ajaxSetup({ async: true });

form.addEventListener('submit',function(event){
	event.preventDefault();

	addToList(input.value, true);
	input.value = "";

});

btn_clear.addEventListener('click',function(){
	localStorage.clear();
	while(addresslist.firstChild){
		addresslist.removeChild(addresslist.firstChild);
	}
	total_sum = 0;
	addresses = [];
	total_balance.textContent = 0;
});