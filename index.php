<?php
if($_SERVER["HTTPS"] != "on")
{
	header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
	exit();
}
?>
<!doctype html>
<html lang="nl">
<head>
	<meta charset=utf-8>
	<title>Divi Balance Tool</title>

	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name='viewport' content='initial-scale=1, viewport-fit=cover'>

	<link href="https://fonts.googleapis.com/css?family=Poppins|Quicksand" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
	<link rel="stylesheet" href="css/style.css?foobar=<?php echo microtime()?>">
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-2853212-25"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-2853212-25');
	</script>

</head>
<body>

	<header class="wrapper">
		<div id="donate"></div>
		<h1><i class="fas fa-robot"></i><div>Divi Balance Tool</div></h1>
	</header>
	<section class="wrapper">
		<div class="space">
			<form id="address-form">
				<div class="row">
					<label for="newaddr">Add a new Divi address to check:</label>
					<input type="text" name="newaddr" id="newaddr" class="divi-addr" placeholder="Copy-paste a Divi address and press «enter»" required>
				</div>
				<div class="row">
					<button type="submit" class="blue"><i class="fas fa-plus-circle"></i> Add address & retrieve balance</button>
				</div>
			</form>
			<h2>Balance info: <span class="right"><i class="fas fa-wallet"></i> <span id="total_balance">0</span></span></h2>
			<p><i class="fas fa-info-circle"></i> Below you'll see the addresses that you've added. Balances will be fetched from diviscan. Click on each address to view it on diviscan.io.</p>
			<ul id="addresses"></ul>
			<button id="btn_clear" class="orange"><i class="fas fa-broom"></i> Clear this list</button>
		</div>
		<div class="space">
			<p><i class="fas fa-info-circle"></i> Use this tool to check the total sum of balances from different Divi addresses. Your list of addresses will only be saved on your device <i class="far fa-smile-beam"></i> <i class="far fa-thumbs-up"></i>. Don't believe me? Have a look at <a href="https://bitbucket.org/jaapvdveen/divi-balance-tool" title="Bitbucket repository" target="_blank"><i class="fab fa-bitbucket"></i> the source</a>.</p>
		</div>
	</section>
	<footer>
		<a id="paypal" href="https://paypal.me/jaapvdveen" title="Hey ya greedy wanker! C'mon! Buy me a beer!" class="donation"><i class="fab fa-paypal"></i> <span class="label">Buy me a beer to say "Thank you Dear!</span></a><br />
		<img src="divi-logo-dark.png" style="height:16px;"> <span title="Donate to my Divi address (-:">DPrzLRuDr5VLAozbahc3XEQUNkBdKAfVGB</span><br />
		<i class="fas fa-copyright"></i> <a href="https://www.jaapvdveen.nl" target="_blank" class="imDoneWithItThisIsTheLastThing smaller">JvdV <i class="fas fa-external-link-alt"></i></a><br/>
		<span class="smaller">No rights can be derived from using this tool.</span>
	</footer>
	<script src="js/app.js?foobar=<?php echo microtime()?>"></script>
</body>
</html>